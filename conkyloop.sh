#!/bin/bash

for((i=1;i<= $(nproc);i++)) 
 do 
	 echo "\${color1}\${goto 35}Core $i : \${color}\${freq_g $i}GHz \${color}\${exec sensors | grep 'Core $((i-1))' | grep  -o '+.*C '} \${alignr}\${cpu cpu$((i-1))}% \${cpubar cpu$((i-1)) 4,100}"
 done
